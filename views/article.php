<!DOCTYPE html>
<html>
<head>
	<title>TestBlog</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
	<div class="layout">
		<div class="layout__row layout__row_services">
			<div id="TMpanel">
				<div class="container">
					<div class="bmenu">
						<a href="#" class="current">Хабр</a>
						<a href="#">Geektimes</a>
						<a href="#">Тостер</a>
						<a href="#">Мой круг</a>
						<a href="#">Фрилансим</a>
					</div>
					<div class="bmenu_inner" style="display:inline-block!important;visibility:visible!important;">
						<span class="bmenu_label">Мегапосты</span>
						<span class="bmenu slink">
							<a href="#" target="_blank" style="color: #3c86ee" rel=" noopener">Эволюция гаджетов</a>
							<a href="#" target="_blank" style="color: #31B007" rel=" noopener">Алюминиевый пост</a>
							<a href="#" target="_blank" style="color: #00008B" rel=" noopener">Мозг играет</a>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="layout__row layout__row_navbar">
			<div class="layout_cell">
				<div class="main-navbar">
					<div class="main-navbar__section main-navbar__section_left">
						<span class="logo-wrapper"><a href="../../blog">TestBlog</a></span>
						<ul class="nav-links" id="navbar-links">
							<li class="nav-links__item">
								<a href="#" class="nav-links__item-link nav-links__item-link_current">Публикации</a>
							</li>
							<li class="nav-links__item">
								<a href="#" class="nav-links__item-link">Пользователи</a>
							</li>
							<li class="nav-links__item">
								<a href="#" class="nav-links__item-link">Хабы</a>
							</li>
							<li class="nav-links__item">
								<a href="#" class="nav-links__item-link">Компании</a>
							</li>
							<li class="nav-links__item">
								<a href="#" class="nav-links__item-link">Песочница</a>
							</li>
						</ul>
						<div class="right-nav">
							<div class="search">
								<button type="button" class="btn">
									<a href="#">
										<img src="images/search.png" width="18px" height="18px">
									</a>
								</button>
							</div>
							<div class="globe">
								<button type="button" class="btn">
									<a href="">
										<img src="images/grid-world.png" width="18px" height="18px">
									</a>
								</button>
							</div>
							<div class="login">
								<button type="button" class="btn btn-login">
									<a href="admin">Админка</a>
								</button>
							</div>
							<div class="registration">
								<button type="button" class="btn btn-primary">Регистрация</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		<div class ="article_item">
			<h3><?=$article['title']?></h3>
			<em>Опубликовано: <?=$article['date']?></em>
			<br>
<<<<<<< HEAD
			<p>Категория: <a href="view_cat.php?id=<?=$article['id']?>&cat=<?=$article['categories']?>"><?=$article['categories']?></a></p>
=======
>>>>>>> c6c582e6fc84977af11a7422ea46f5864588fa49
			<br>
			<p><?=$article['content']?></p>
			<button type="button" name="button" class="btn btn-success main_return">
				<a href="../../blog" class="add_article">На главную</a>
			</button>
		</div>
		<footer>
			<p>Мой первый блог<br>Copyrigth &copy;2019</p>
		</footer>
</body>
</html>
