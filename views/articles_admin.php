<!DOCTYPE html>
<html>
<head>
	<title>TestBlog</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container_main">
		<h1>Test Blog</h1>
		<div class="articles_admin">
			<button type="button" name="button" class="btn btn-primary add_button">
				<a href="index.php?action=add" class="add_article">Добавить статью</a>
			</button>
			<button type="button" name="button" class="btn btn-success main_return">
				<a href="../../blog" class="add_article">На главную</a>
			</button>
			<table class="admin-table" border="2px">
				<tr>
					<th>Дата</th>
					<th>Заголовок</th>
					<th></th>
					<th></th>
				</tr>
			<?php foreach($articles as $a): ?>
				<tr>
					<td><?=$a['date']?></td>
					<td><?=article_headline($a['title'])?></td>
					<td>
						<a href="index.php?action=edit&id=<?=$a['id']?>">Редактировать</a>
					</td>
					<td>
						<a href="index.php?action=delete&id=<?=$a['id']?>">Удалить</a>
					</td>
				</tr>
			<?php endforeach ?>
			</table>
		</div>
		<footer>
			<p>Мой первый блог<br>Copyrigth &copy;2019</p>
		</footer>
	</div>
</body>
</html>
