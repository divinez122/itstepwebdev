<!DOCTYPE html>
<html>
<head>
	<title>TestBlog</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<center><h1>Test Blog</h1></center>
		<div class="main_form">
			<form method="post" action="index.php?action=add" class="add_delete">
				<label>
					Название
					<input type="text" name="title" value="<?=$article['title']?>" class="form-item" autofocus required>
				</label>
				<label>
					Дата
					<input type="date" name="date" value="<?=$article['date']?>" class="form-item_date" autofocus required>
				</label>
				<label>
					Содержимое
					<textarea class="form-item_textarea" rows="7" name="content" required><?=$article['content']?></textarea>
				</label>
				<label>
					Категория
					<input type="text" name="categories" value="<?=$article['categories']?>" class="form-item" autofocus required>
				</label>
				<input type="submit" value="Сохранить" class="btn btn-primary submit_button">
				<a href="/../blog/admin" class="btn btn-success return">Назад</a>
			</form>
		</div>
		<footer>
			<p>Мой первый блог<br>Copyrigth &copy;2019</p>
		</footer>
	</div>
</body>
</html>
