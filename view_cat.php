<?php
	require_once("database.php");
	require_once("models/articles.php");

	$link = db_connect();
	$article = articles_get($link, $_GET['id']);
	$cat = getByCategories($article["categories"]);
	include("views/cat_article.php");
 ?>
